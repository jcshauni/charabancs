const sections = [
  { title: 'Accueil', url: '/' },
  { title: 'Administration', url: '/about' },
  { title: 'Récompenses', url: '/recompenses' },
  { title: 'Contact', url: '/contact' },
  { title: 'Planning', url: '/planning' },
];

export default sections;
