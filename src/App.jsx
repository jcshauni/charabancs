import Container from '@material-ui/core/Container';
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import Blog from './Blog';
import About from './Static/About';
import Contact from './Static/Contact';
import Recompenses from './Static/Recompenses';
import Planning from './Static/Planning';

const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
  backgoundApp: {
    backgroundImage: 'url(background.jpg)',
    backgroundAttachment: 'fixed',
    backgroundSize: '100%',
    height: '100%',
  },
  content: {
    background: '#f0f0f0',
    maxWidth: '66%',
  },
}));

export default function App() {
  const classes = useStyles();
  return (
    <Router>
      <Switch>
        <div className={classes.backgoundApp}>
          <Container className={classes.content}>
            <Route exact path="/">
              <Blog />
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/contact">
              <Contact />
            </Route>
            <Route path="/recompenses">
              <Recompenses />
            </Route>
            <Route path="/planning">
              <Planning />
            </Route>
          </Container>
        </div>
      </Switch>
    </Router>
  );
}
