import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import React from 'react';
// import { Container } from '@material-ui/core';
import FeaturedPost from './FeaturedPost';
import Header from './Header';
import MainFeaturedPost from './MainFeaturedPost';
import sections from './Shared/Shared';
import Sidebar from './Sidebar';
import { mainFeaturedPost, featuredPosts } from './Posts/posts';
import Description from './Static/Description';

const sidebar = {
  social: [
    { name: 'Twitter', icon: TwitterIcon },
    { name: 'Facebook', icon: FacebookIcon },
  ],
};
const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
}));

export default function Blog() {
  const classes = useStyles();

  return (
    <div>
      <CssBaseline />
      <Header title="Association Charabancs Annecy" sections={sections} />
      <main>
        <MainFeaturedPost post={mainFeaturedPost} />
        <Description />
        <Grid container spacing={4}>
          {featuredPosts.map((post) => (
            <FeaturedPost key={post.title} post={post} />
          ))}
        </Grid>
        <Grid container spacing={5} className={classes.mainGrid}>
          <Sidebar social={sidebar.social} />
        </Grid>
      </main>
    </div>
  );
}
