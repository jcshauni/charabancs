export const mainFeaturedPost = {
  title: 'Les Charabancs en confinements',
  description:
      "Parce qu'être confiné ne signifie pas être oisif, les Charabancs proposent des activités en ligne.",
  image: 'images/tasse_jeanne.jpeg',
  images: [
    'annie_arbre.jpeg',
    'josette.jpeg',
    'odile_arbre.jpeg',
    'brigitte.jpeg',
    'liprandi_B.jpeg',
    'odile_mandarine.jpg',
    'bri.jpeg',
    'mandarine_Nicole.jpg',
    'recette_jeanne.jpeg',
    'confinement.jpeg',
    'marc2_arbre.jpeg',
    'recette_odile.jpeg',
    'francine_arbre.jpeg',
    'marc.jpeg',
    'tasse_jeanne.jpeg',
    'jeanne_Arbre.jpeg',
    'marc_recette.jpeg',
    'jeanne_homme.jpeg',
    'nicole_recette.jpg',
  ],
  imgText:
      'Vous êtes libre de créer, transformer, recycler, réaliser toutes vos envies.',
  linkText: 'Lire la suite',
  descriptionLongue: `<p>Lors du confinement, nous proposons des activités en ligne pouvant être suivies avec un compte zoom (application gratuite téléchargeable <a href=www.zoom.fr>ici</a>)</p><br/>
      La liste des activités est la suivante : <br/>
      <ul>
        <li>Peinture par Marie Alice Carpentier</li>
        <li>Pilates par Isabelle</li>
        <li>Art floral par Nadia</li>
      </ul>
      <p>L'organisation est effectuée par les professeurs, voici quelques exemples des créations de nos élèves en cours à distance</p>`,
};
export const featuredPosts = [
  {
    title: 'Claquettes',
    date: 'Vendredi, 14 à 16 heures',
    description:
        'Claudette vous propose de découvrir ce style de danse, qui ne nécessite pas de partenaire, dans un esprit décontracté et dynamique.',
    image:
        'https://charabancs.files.wordpress.com/2020/02/5e19415842d04492956eb91ceaf13f781.jpg',
    imageText: 'Image Text',
    descriptionLongue: `<p>Les claquettes (tap dance en anglais) sont un style de danse né aux États-Unis au XIXe siécle. 
        Le nom de claquettes vient du son produit par des fers (morceaux de métal adaptés) fixés sous les chaussures, ce qui fait de celui-ci, en même temps qu’un danseur, un percussionniste.</p> 
        <p>Après une partie échauffement et assouplissement musculaire, vous apprendrez les différentes « frappes » lors d’exercices techniques, 
        puis mise en application dans une chorégraphie musicale entrainante, pour le plaisir de la danse, 
        ce que sont avant tout les claquettes. </p>
        <p>Vous n’aurez besoin que d’une simple tenue de ville confortable, et d’une paire de chaussures de claquettes (pour le 1er cours de découverte, des chaussures de ville confortables sans talon feront l’affaire).</p> 
        <p>Claudette danse depuis plus de 20 ans (danse sportive, modern jazz, danses swing et latines), et fais également des claquettes depuis 7 ans, et très vite devenue passionnée. </p>
        <p>Et dans le but de vous faire découvrir cette passion, Claudette a suivi durant 2 ans une formation auprès de son professeur de claquettes, et continue à prendre des cours pour son plaisir et continue à évoluer.</p>`,
    images: [
      {
        original: 'https://i.imgur.com/mbFQgqb.jpg',
      },
      {
        original: 'https://i.imgur.com/bqxblxr.jpg',
      },
      {
        original: 'https://i.imgur.com/7BBYq3c.jpg',
      },
      {
        original: 'https://i.imgur.com/E6Ih1Fv.jpg',
      },
      {
        original: 'https://i.imgur.com/GVMj2jR.jpg',
      },
    ],
  },
  {
    title: 'Informatique',
    date: 'Vendredi, 9h à 11h30',
    description: `L’objectif de l’activité informatique est de transmettre le fonctionnement des outils
        informatiques (ordinateurs, tablettes, Smartphones) et apprendre leur utilisation.`,
    image: 'https://charabancs.files.wordpress.com/2020/02/p1150874.jpg?w=1024',
    imageText: 'Image Text',
    descriptionLongue: `<p>Différentes formations sont dispensées : </p> 
        <p>Création d’une adresse de messagerie, d’un compte Microsoft, connaître le
        fonctionnement d’Internet et comment se protéger, comment compresser un fichier,
        stockage des données, travail sur tous sur les logiciels de bureautique (traitement de texte,
        tableur, présentation, diaporama avec Microsoft office et Libre office) et réalisation
        d’album photos…</p>
        <p>les 4 intervenants sont là pour répondre à toutes vos questions et résoudre les problèmes de
        logiciel, de matériel …</p>`,
    images: [
      {
        original: 'https://i.imgur.com/IgEqe4q.png',
      },
      {
        original: 'https://i.imgur.com/aAeYm6D.png',
      },
      {
        original: 'https://i.imgur.com/CXmZTzo.png',
      },
      {
        original: 'https://i.imgur.com/L4qDGga.png',
      },
      {
        original: 'https://i.imgur.com/9KCWDER.png',
      },
    ],
  },
  {
    title: 'Peinture du vendredi',
    date: 'Vendredi, 17h15 à 19h',
    description: 'Hervé aime à faire partager sa passion, ses connaissances',
    image: 'https://charabancs.files.wordpress.com/2018/05/p1110425.jpg?w=300',
    imageText: 'Image Text',
    descriptionLongue: `<p>Hervé Viguié, peintre depuis plus de 30 ans, vous accompagne dans une ambiance détendue,
        pour débuter ou perfectionner votre technique : crayon, aquarelle, huile, acrylique…</p> 
        <p>Pas de cours magistral, ni de sujet imposé, mais une aide personnalisée, pour que chacun
        progresse à son rythme, avec les problématiques qui lui sont propre.</p>
        <p>Durant l&#39;année, un ou deux sujets seront tout de même réalisés en commun, pour s&#39;initier à de
        nouvelles techniques.</p>
        <p>Chacun apporte son matériel, il est possible de le laisser sur place dans une salle fermée.
        L&#39;association peut également fournir du matériel de dépannage.</p>`,
    images: [
      {
        original: 'https://i.imgur.com/lmbTmpm.png',
      },
      {
        original: 'https://i.imgur.com/jfqf2jk.png',
      },
      {
        original: 'https://i.imgur.com/ImF4Ssa.png',
      },
      {
        original: 'https://i.imgur.com/LR83YDZ.png',
      },
    ],
  },
  {
    title: 'Peinture du jeudi',
    date: 'Jeudi, 14h à 17h',
    description:
        'Marie-Alice , passionnée par l’art propose des cours de dessin, pastel et toute autre technique de peinture.',
    descriptionLongue: `<p>Les cours sont d’une durée de 3 heures par semaine. 
        C’est un lieu de convivialité, où chacun avance à son rythme, dans la technique de son choix : le pastel sec, mais aussi l'acrylique, l'huile ou l'aquarelle. 
        Une grande importance au dessin peut aussi être un atout pour bien progresser et comprendre les valeurs d'un sujet. 
        Par une approche ludique, vous pourrez apprendre les bases du dessin.</p>
        <p>Cette activité peut être pratiquée par tous, quels que soient, au départ, les dons pour le dessin.
  
        Nous avons tous un potentiel créateur que l’on peut développer avec un peu de technique et surtout le goût d’apprendre</p>`,
    image:
        'https://charabancs.files.wordpress.com/2018/04/peinture-jeudi-1.jpg',
    imageText: 'Image Text',
    images: [
      {
        original: 'https://i.imgur.com/X75JiqU.png',
      },
      {
        original: 'https://i.imgur.com/qSxNcS1.png',
      },
      {
        original: 'https://i.imgur.com/fJPQHOR.png',
      },
      {
        original: 'https://i.imgur.com/HNml66h.png',
      },
      {
        original: 'https://i.imgur.com/DHWcH2w.png',
      },
    ],
  },
  {
    title: 'Couture',
    date: 'Lundi, 14h à 16h30',
    description: 'Danièle dynamique intervenante vous accompagne pour perfectionner votre pratique en couture.',
    descriptionLongue: `<p>Danielle vous aide à la création, à la transformation de vos vêtements, vous conseille
        pour la réalisation de tous vos projets personnels.</p>
        <p>Vous êtes tout à fait libre de créer vêtement, sac, pochette, de recycler, de réaliser toutes
        vos envies.</p>
        <p>Il suffit de venir avec votre projet, votre matériel, votre tissu et l’indispensable machine à
        coudre.</p>`,
    image: 'https://i.imgur.com/4qIJRdx.png',
    imageText: 'Image Text',
    images: [
      {
        original: 'https://i.imgur.com/jAqrrUd.jpg',
      },
      {
        original: 'https://i.imgur.com/4qIJRdx.png',
      },
    ],
  },
  {
    title: 'Art floral',
    date: '1 fois par mois',
    description: `L’atelier d’Art floral animé par Nadia vous initie à la création de tous types de
        bouquets, de compositions.`,
    descriptionLongue: `<p>Venez vous détendre et réaliser votre composition florale.</p>
        <p>Ensemble, vous définissez le thème de la séance, bien souvent en fonction du
        calendrier : Pâques, Printemps, Fête des mères, Noël…</p>
        <p>Nadia fournit les fleurs, la mousse, vous guide pas à pas pour la réalisation de bouquets
        ronds, de bouquets hauts, de centres de table, de couronne de porte…</p>
        <p>Elle vous assiste de ses précieux conseils tout au long de la séance, séance photo et bien sûr, retour à la maison avec votre composition !`,
    image: 'https://i.imgur.com/mbAwaB1.jpg',
    imageText: 'Image Text',
    images: [
      {
        original: 'https://i.imgur.com/8QfB4Dh.jpg',
      },
      {
        original: 'https://i.imgur.com/uf23vRl.jpg',
      },
      {
        original: 'https://i.imgur.com/0f86CAj.jpg',
      },
      {
        original: 'https://i.imgur.com/2VBnkyE.jpg',
      },
      {
        original: 'https://i.imgur.com/Lofviwa.jpg',
      },
      {
        original: 'https://i.imgur.com/0MCCkqy.jpg',
      },
      {
        original: 'https://i.imgur.com/mbAwaB1.jpg',
      },
      {
        original: 'https://i.imgur.com/wgxfiR9.jpg',
      },
      {
        original: 'https://i.imgur.com/7KrfykY.jpg',
      },
      {
        original: 'https://i.imgur.com/s82Ja0a.jpg',
      },
      {
        original: 'https://i.imgur.com/G0F2xAg.jpg',
      },
      {
        original: 'https://i.imgur.com/PJIzUw7.jpg',
      },
    ],
  },
];
