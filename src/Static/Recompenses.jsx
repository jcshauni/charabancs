import {
  Container,
  CssBaseline,
  Grid,
  makeStyles,
  Paper,
  Typography,
} from '@material-ui/core';
import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import Header from '../Header';
import sections from '../Shared/Shared';

const useStyles = makeStyles((theme) => ({
  historyPost: {
    position: 'relative',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(4),
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  historyContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
}));

export default function Recompenses() {
  const classes = useStyles();
  return (
    <>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="Association Charabancs Annecy" sections={sections} />
        <main>
          <Paper
            className={classes.historyPost}
            style={{
              backgroundImage: 'url(\'https://charabancs.files.wordpress.com/2012/04/0471.jpg\'',
            }}
          >
            <img
              style={{ display: 'none' }}
              src="https://charabancs.files.wordpress.com/2012/04/0471.jpg"
              alt="Charabancs"
            />
            <div className={classes.overlay} />
            <Grid container>
              <Grid item md={6}>
                <div className={classes.historyContent}>
                  <Typography
                    component="h1"
                    variant="h3"
                    color="inherit"
                    gutterBottom
                  >
                    Récompenses
                  </Typography>
                </div>
              </Grid>
            </Grid>
          </Paper>
          <Typography variant="h6">
            Voici la liste des nominations et récompenses obtenues par les éleves
            et professeurs de l&apos;association des Charabancs

          </Typography>
          <Carousel showArrows width="40%" dynamicHeight>
            <div>
              <img src="Recompenses/image1.jpeg" alt="4ème prix Huile et acrylique : Bord de mer  -  Marie Alice CARPENTIER" />
              <p className="legend">4ème prix Huile et acrylique : Bord de mer - Marie Alice CARPENTIER</p>
            </div>
            <div>
              <img src="Recompenses/image2.jpeg" alt="3ème prix : Femme bleue  -   Josette Montaud" />
              <p className="legend">3ème prix : Femme bleue - Josette Montaud</p>
            </div>
            <div>
              <img src="Recompenses/image4.jpeg" alt="2ème prix Aquarelle :   Roue et  vapeur  -   Jean Marie DESRUES" />
              <p className="legend">2ème prix Aquarelle : Roue et  vapeur - Jean Marie DESRUES</p>
            </div>
            <div>
              <img src="Recompenses/image5.jpeg" alt="1er prix Pastel :   Gourmandise  -   Josette MONTAUD" />
              <p className="legend">1er prix Pastel : Gourmandise - Josette MONTAUD</p>
            </div>
            <div>
              <img src="Recompenses/image7.jpeg" alt="2ème prix Aquarelle (ex aequo   :   Tas de bois  -   Jean Marie DESRUES" />
              <p className="legend">2ème prix Aquarelle (ex aequo) : Tas de bois - Jean Marie DESRUES</p>
            </div>
            <div>
              <img src="Recompenses/image8.jpeg" alt="1er prix Aquarelle :  Atelier  -   Marie Alice CARPENTIER" />
              <p className="legend">1er prix Aquarelle : Atelier - Marie Alice CARPENTIER</p>
            </div>
            <div>
              <img src="Recompenses/image9.jpeg" alt="6ème prix Huile et acrylique : Farandole planétaire  -  Annie CHAMPEL" />
              <p className="legend">6ème prix Huile et acrylique : Farandole planétaire - Annie CHAMPEL</p>
            </div>
            <div>
              <img src="Recompenses/image10.jpeg" alt="Caroline DUCREST" />
              <p className="legend">Caroline DUCREST</p>
            </div>
            <div>
              <img src="Recompenses/image11.jpeg" alt="Sélection du concours national 2016 :  Cabane de pêcheurs  -  Marie José BARRIERE" />
              <p className="legend">Sélection du concours national 2016 : Cabane de pêcheurs - Marie José BARRIERE</p>
            </div>
            <div>
              <img src="Recompenses/unnamed.jpeg" alt="2ème prix :  Yvette GHENO" />
              <p className="legend">2ème prix : Yvette GHENO</p>
            </div>
            <div>
              <img src="Recompenses/unnamed2.jpeg" alt="9ème prix Huile et acrylique :  L’été au lac d’Annecy  - Josette MONTAUD" />
              <p className="legend">9ème prix Huile et acrylique : L’été au lac d’Annecy - Josette MONTAUD</p>
            </div>
          </Carousel>
        </main>
      </Container>
    </>
  );
}
