import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@material-ui/core';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import React from 'react';

export default function Description() {
  return (
    <Typography variant="body1">
      <p>
        « Les Charabancs » est une association Annécienne affiliée au réseau
        UAICF. (Union Artistique Intellectuelle des Cheminots Français).
      </p>
      <p>
        Son nom fait référence à l’activité théâtre (activité pratiquée au
        moment de sa création en 1989).
      </p>
      <p>
        En effet, le mot char à bancs, désigne une voiture longue et légère
        garnie de bancs et ouverte de tous côtés ou fermée simplement de
        rideaux. (Définition selon le dictionnaire de français « Littré »).
      </p>
      <p>A l’heure actuelle, il n’y a plus de section théâtre.</p>
      <p>
        L&apos;association propose des activités culturelles pour les agents
        SNCF actifs, retraités, pour leurs familles mais accueille également des
        personnes extérieures.
      </p>
      <p> Nous proposons les activités suivantes :</p>
      <List dense>
        <ListItem>
          <ListItemIcon>
            <ArrowRightIcon />
          </ListItemIcon>
          <ListItemText primary="Pilates" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <ArrowRightIcon />
          </ListItemIcon>
          <ListItemText primary="Peinture du Jeudi" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <ArrowRightIcon />
          </ListItemIcon>
          <ListItemText primary="Peinture du vendredi" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <ArrowRightIcon />
          </ListItemIcon>
          <ListItemText primary="Art floral" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <ArrowRightIcon />
          </ListItemIcon>
          <ListItemText primary="Informatique" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <ArrowRightIcon />
          </ListItemIcon>
          <ListItemText primary="Danse Claquettes" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <ArrowRightIcon />
          </ListItemIcon>
          <ListItemText primary="Couture et Musique" />
        </ListItem>
      </List>
    </Typography>
  );
}
