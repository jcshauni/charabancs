import {
  Container,
  CssBaseline,
  Grid,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Paper,
  Typography,
} from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import Header from '../Header';
import sections from '../Shared/Shared';

const REACT_APP_API_KEY = process.env.REACT_APP_API_KEY || '';
const mapStyles = {
  width: '30%',
  height: '50vh',
};

const positions = {
  lat: 45.90183946701791,
  long: 6.1151408372743905,
};

const useStyles = makeStyles((theme) => ({
  historyPost: {
    position: 'relative',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(4),
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  historyContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
}));

export function Contact({ google }) {
  const classes = useStyles();
  return (
    <>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="Association Charabancs Annecy" sections={sections} />
        <main>
          <Paper
            className={classes.historyPost}
            style={{
              backgroundImage: 'url(\'https://charabancs.files.wordpress.com/2012/04/0471.jpg\'',
            }}
          >
            <img
              style={{ display: 'none' }}
              src="https://charabancs.files.wordpress.com/2012/04/0471.jpg"
              alt="Charabancs"
            />
            <div className={classes.overlay} />
            <Grid container>
              <Grid item md={6}>
                <div className={classes.historyContent}>
                  <Typography
                    component="h1"
                    variant="h3"
                    color="inherit"
                    gutterBottom
                  >
                    Contact
                  </Typography>
                </div>
              </Grid>
            </Grid>
          </Paper>
          <Typography variant="body1">
            Une question, une demande ? N’hésitez pas à nous contacter par email
            ou par téléphone :
            <List>
              <ListItem>
                <ListItemText>
                  <a href="mailto:charabancs@gmail.com">charabancs@gmail.com</a>
                </ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText>
                  <strong>Notre adresse</strong>
                  <br />
                  Les charabancs
                  <br />
                  7 rues des usines
                  <br />
                  74000 Annecy
                  <br />
                  07 81 53 01 75
                </ListItemText>
              </ListItem>
            </List>
          </Typography>
          <Map
            google={google}
            style={mapStyles}
            initialCenter={
          {
            lat: positions.lat,
            lng: positions.long,
          }
        }
          >
            <Marker
              mapCenter={{ lat: positions.lat, lng: positions.long }}
            />
          </Map>
        </main>
      </Container>
    </>
  );
}

Contact.propTypes = {
  google: PropTypes.shape({
    what: PropTypes.string,
  }),
};

export default GoogleApiWrapper({
  apiKey: REACT_APP_API_KEY,
})(Contact);
