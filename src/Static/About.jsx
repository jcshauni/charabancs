import {
  Container,
  CssBaseline,
  Grid,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Paper,
  Typography,
} from '@material-ui/core';
import React from 'react';
import Header from '../Header';
import sections from '../Shared/Shared';

const useStyles = makeStyles((theme) => ({
  aboutPost: {
    position: 'relative',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(4),
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  aboutContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
}));

export default function About() {
  const classes = useStyles();
  return (
    <>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="Association Charabancs Annecy" sections={sections} />
        <main>
          <Paper
            className={classes.aboutPost}
            style={{
              backgroundImage: 'url(\'https://charabancs.files.wordpress.com/2012/04/0471.jpg\'',
            }}
          >
            <img
              style={{ display: 'none' }}
              src="https://charabancs.files.wordpress.com/2012/04/0471.jpg"
              alt="Charabancs"
            />
            <div className={classes.overlay} />
            <Grid container>
              <Grid item md={6} spacing={2}>
                <div className={classes.aboutContent}>
                  <Typography
                    component="h1"
                    variant="h3"
                    color="inherit"
                    gutterBottom
                  >
                    Qui sommes nous ?
                  </Typography>
                </div>
              </Grid>
            </Grid>
          </Paper>
          <Typography variant="body1">
            « Les Charabancs » est une association culturelle Annécienne,
            faisant partie du réseau UAICF. (Union Artistique Intellectuelle des
            Cheminots Français)
          </Typography>
          <Grid container item md={6} spacing={4}>
            <Grid item md={6}>
              <Typography variant="h6">MEMBRES DU BUREAU</Typography>
              <List dense>
                <ListItem>
                  <ListItemText
                    primary="Gisèle ROUGER"
                    secondary="Présidente"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Chantal BOLATTO"
                    secondary="Vice-Présidente"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Guy BERUARD" secondary="Trésorier" />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Kamel BOUROU"
                    secondary="Trésorier adjoint"
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Francine BONNIN"
                    secondary="Secrétaire"
                  />
                </ListItem>
              </List>
            </Grid>
            <Grid item md={6}>
              <Typography variant="h6">CONSEIL D&apos;ADMINISTRATION</Typography>
              <List dense>
                <ListItem>
                  <ListItemText primary="Patricia JACCOUD" />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Monique MAIGNAN" />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Annie MAIREY" />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Daniel MOLLIEX" />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Josette MONTAUD" />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Catherine PERRIN" />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Odile TREVISAN" />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Marc WINNICKI" />
                </ListItem>
              </List>
            </Grid>
          </Grid>

          <Typography variant="body1">
            Nous vous proposons plusieurs type d’activités : Art Floral
            Différents cours de peinture (Aquarelle, acrylique, huile, pastel,
            dessin) Gym Pilates Musique Claquettes Couture Informatique Pour
            découvrir les enseignements proposés, surfer sur ce blog.
          </Typography>
        </main>
      </Container>
    </>
  );
}
