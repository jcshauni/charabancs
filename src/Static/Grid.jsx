import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 13,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function CustomizedTables() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell />
            <StyledTableCell align="center">Couture</StyledTableCell>
            <StyledTableCell align="center">Art Floral</StyledTableCell>
            <StyledTableCell align="center" colSpan="2">Pilates</StyledTableCell>
            <StyledTableCell align="center">Peinture Jeudi</StyledTableCell>
            <StyledTableCell align="center">Musique</StyledTableCell>
            <StyledTableCell align="center">Peinture Vendredi</StyledTableCell>
            <StyledTableCell align="center">Initiation Claquettes</StyledTableCell>
            <StyledTableCell align="center">Informatique</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <StyledTableRow>
            <StyledTableCell component="th">Jour</StyledTableCell>
            <StyledTableCell align="center">Lundi</StyledTableCell>
            <StyledTableCell align="center">Mardi</StyledTableCell>
            <StyledTableCell align="center">Mardi</StyledTableCell>
            <StyledTableCell align="center">Jeudi</StyledTableCell>
            <StyledTableCell align="center">Jeudi</StyledTableCell>
            <StyledTableCell align="center">Jeudi</StyledTableCell>
            <StyledTableCell align="center">Vendredi</StyledTableCell>
            <StyledTableCell align="center">Vendredi</StyledTableCell>
            <StyledTableCell align="center">Vendredi</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th">Animateur</StyledTableCell>
            <StyledTableCell align="center">Danielle PERRIER</StyledTableCell>
            <StyledTableCell align="center">Nadia BELFER</StyledTableCell>
            <StyledTableCell align="center" colSpan="2">Isabelle ROSSI</StyledTableCell>
            <StyledTableCell align="center">Marie-Alice CARPENTIER</StyledTableCell>
            <StyledTableCell align="center">Olivier GEAY</StyledTableCell>
            <StyledTableCell align="center">Hervé VIGUIE</StyledTableCell>
            <StyledTableCell align="center">Claudette COMBES</StyledTableCell>
            <StyledTableCell align="center">Guy BERUARD</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow rowSpan="3">
            <StyledTableCell component="th">Horaires</StyledTableCell>
            <StyledTableCell align="center">14:00 à 16:00</StyledTableCell>
            <StyledTableCell align="center">18:30 à 20:30</StyledTableCell>
            <StyledTableCell align="center">
              11:00 à 12:00
              <br />
              12:15 à 13:15
              <br />
              17:15 à 18:15
            </StyledTableCell>
            <StyledTableCell align="center">
              11:00 à 12:00
              <br />
              12:15 à 13:15
            </StyledTableCell>
            <StyledTableCell align="center">20:30 à 22:30</StyledTableCell>
            <StyledTableCell align="center">14:00 à 17:00</StyledTableCell>
            <StyledTableCell align="center">18:00 à 20:15</StyledTableCell>
            <StyledTableCell align="center">14:00 à 16:00</StyledTableCell>
            <StyledTableCell align="center">09:00 à 12:00</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th">Dates</StyledTableCell>
            <StyledTableCell align="center">Reprise le 07/09/20</StyledTableCell>
            <StyledTableCell align="center">Reprise le 06/10/20</StyledTableCell>
            <StyledTableCell align="center">Reprise le 15/09/20</StyledTableCell>
            <StyledTableCell align="center">Reprise le 17/10/20</StyledTableCell>
            <StyledTableCell align="center">Reprise le 17/09/20</StyledTableCell>
            <StyledTableCell align="center">Reprise le 17/09/20</StyledTableCell>
            <StyledTableCell align="center">Reprise le 18/09/20</StyledTableCell>
            <StyledTableCell align="center">Reprise le 25/09/20</StyledTableCell>
            <StyledTableCell align="center">Reprise en Janvier 2021</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th">Janvier</StyledTableCell>
            <StyledTableCell align="center" rowSpan="5">Tous les lundis hors vacances scolaires</StyledTableCell>
            <StyledTableCell align="center" rowSpan="5">06/12/20</StyledTableCell>
            <StyledTableCell align="center" rowSpan="5">Tous les mardis hors vacances scolaires</StyledTableCell>
            <StyledTableCell align="center" rowSpan="5">Tous les jeudis hors vacances scolaires</StyledTableCell>
            <StyledTableCell align="center">07-14-21-28</StyledTableCell>
            <StyledTableCell align="center" rowSpan="5">Tous les jeudis hors vacances scolaires</StyledTableCell>
            <StyledTableCell align="center">08-15-22-29</StyledTableCell>
            <StyledTableCell align="center" rowSpan="5">Tous les vendredis hors vacances scolaires</StyledTableCell>
            <StyledTableCell align="center" rowSpan="5">Tous les vendredis</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th">Février</StyledTableCell>
            <StyledTableCell align="center">04-25</StyledTableCell>
            <StyledTableCell align="center">05-26</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th">Mars</StyledTableCell>
            <StyledTableCell align="center">04-11-18-25</StyledTableCell>
            <StyledTableCell align="center">05-12-19-26</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th">Avril</StyledTableCell>
            <StyledTableCell align="center">01-08-29</StyledTableCell>
            <StyledTableCell align="center">02-09-30</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th">Mai</StyledTableCell>
            <StyledTableCell align="center">06-20-27</StyledTableCell>
            <StyledTableCell align="center">07-21-28</StyledTableCell>
          </StyledTableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}
