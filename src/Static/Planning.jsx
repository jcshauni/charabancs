import React from 'react';
import {
  CssBaseline,
  Container,
  Paper,
  Grid,
  Typography,
  makeStyles,
} from '@material-ui/core';
import Header from '../Header';
import sections from '../Shared/Shared';
import Grille from './Grid';

const useStyles = makeStyles((theme) => ({
  historyPost: {
    position: 'relative',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(4),
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  historyContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
}));

export default function Planning() {
  const classes = useStyles();
  return (
    <>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="Association Charabancs Annecy" sections={sections} />
        <main>
          <Paper
            className={classes.historyPost}
            style={{
              backgroundImage: 'url(\'https://charabancs.files.wordpress.com/2012/04/0471.jpg\'',
            }}
          >
            <img
              style={{ display: 'none' }}
              src="https://charabancs.files.wordpress.com/2012/04/0471.jpg"
              alt="Charabancs"
            />
            <div className={classes.overlay} />
            <Grid container>
              <Grid item md={6}>
                <div className={classes.historyContent}>
                  <Typography
                    component="h1"
                    variant="h3"
                    color="inherit"
                    gutterBottom
                  >
                    Planning
                  </Typography>
                </div>
              </Grid>
            </Grid>
          </Paper>
          <Typography variant="body1">
            Voici le planning des activités des Charabancs pendant la semaine
          </Typography>
          <Grille />
        </main>
      </Container>
    </>
  );
}

Planning.propTypes = {};
